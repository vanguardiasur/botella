--[[
Name	: ircbot.lua -- fast, diverse irc bot in lua
Author	: David Shaw (dshaw@redspin.com)
Date	: August 8, 2010
Desc.	: ircbot.lua uses the luasocket library. This
	  can be installed on Debian-based OS's with
	  sudo apt-get install liblua5.1-socket2.
	
License	: BSD License
Copyright (c) 2010, David Shaw
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

]]--

socket = require("socket")
http = require("socket.http")
lineregex = "[^\r\n]+"
verbose = false
mynick = 'botella'
socialness = 0.2

local toariel_talk = {
	"noticias del fonsoft?",
	"y cómo va la tesina?",
}

local pong_talk = {
	"pong",
	"pong",
	"pong",
	"que pasa?",
	"acá estoy",
	"ya voy - dame un segundo",
	"ya voy - dame un segundito que termino una cosa",
}

local jaja_talk = {
	"deberías reírte menos y trabajar más",
	"yo que vos no me reiría tanto",
	"yo no me reiría tanto",
	"no le veo la gracia",
	"jajaja",
	"jaaa",
	"jaja",
	"ja",
	"XD",
}

local happy_talk = {
	"te escucho motivado",
	"están motivados!",
	"bien ahí",
}

local random_talk = {
	"hm, esto se está volviendo surrealista",
	"las cosas que hay que escuchar...",
	"no entendí eso",
	"está bien eso?",
	"hmm",
}

local question_yes_no = {
	"la verdad, ni idea",
	"me parece que si",
	"no estoy segura",
	"para mi que si",
	"para mi que no",
	"creo que si",
	"creo que no",
	"ni idea",
	"no creo",
}

function deliver(s, content)
	s:send(content .. "\r\n\r\n")
end

function sleep(sec)
    socket.select(nil, nil, sec)
end

function talk(s, channel, content)
	-- after a 5 year research, we came to this formulae
	local human_delay = math.random() * 1.0 + content:len() / 20.0 + 0.8
	sleep(human_delay)
	deliver(s, "PRIVMSG " .. channel .. " :" .. content)
end

function msg(s, channel, content)
	deliver(s, "PRIVMSG " .. channel .. " :" .. content)
end

function starts_with(str, match)
	return str:sub(1, match:len()) == match
end

function ends_with(str, match)
	return match == '' or str:sub(-match:len()) == match
end

function firstword(str)
	return str:match("(%w+)(.+)")
end

function chitchat(s, channel, fromnick, line, isquery)

	local t

	if isquery then
		if line:find('ping') then
			t = pong_talk[math.random(#pong_talk)]
		elseif ends_with(line, "no?") or ends_with(line, "si?") then
			t = question_yes_no[math.random(#question_yes_no)]
		else
			t = random_talk[math.random(#random_talk)]
		end

	elseif math.random() < socialness then
		if line:find('ja') then
			t = jaja_talk[math.random(#jaja_talk)]
		elseif line:find('contento') or line:find('feliz') or line:find('vamos!') then
			t = happy_talk[math.random(#happy_talk)]
		elseif fromnick:find('ariel') then
			t = toariel_talk[math.random(#toariel_talk)]
		end
	end

	if t then talk(s, channel, t) end
end

-- process needs to process "line" and call higher bot tasks
function process(s, channel, fromnick, line, isquery)

	if isquery then print(line .. " is a query on " .. channel .. " from " .. fromnick) end

	if isquery and line:find("!help") then
		local com = {}
		com[#com + 1] = "--- Help and Usage ---"
		com[#com + 1] = "!google <query> -- returns a Google search - FIXME"
		com[#com + 1] = "!so <query> -- returns a Stack Overflow search - FIXME"
		com[#com + 1] = "!uptime -- returns the server uptime"
		com[#com + 1] = "!morite -- kill me (if you can)"
		for x=1, #com do
			msg(s, channel, com[x])
		end
	elseif isquery and line:find("!morite") then
		if fromnick == "ezequielg" then
			os.exit()
		else
			msg(s, channel, "morite vos!")
		end
	elseif isquery and line:find("!uptime") then
		local f = io.popen("uptime")
		msg(s, channel, fromnick .. ":" .. f:read("*l"))
	else
		chitchat(s, channel, fromnick, line, isquery)
	end
end

function pre_process(receive, channel)
	-- gotta grab the ping "sequence".
	if receive:find("PING :") then
		deliver(s, "PONG :" .. string.sub(receive, (string.find(receive, "PING :") + 6)))
		if verbose then
			print("[+] sent server pong")
		end
	elseif receive:find("JOIN") then
		if receive:find(channel .. " :") then
			line = string.sub(receive, (string.find(receive, channel .. " :") + (#channel) + 2))
		end
		if receive:find(":") and receive:find("!") then
			fromnick = string.sub(receive, (string.find(receive, ":")+1), (string.find(receive, "!")-1))
		end
		if fromnick ~= mynick then
			talk(s, channel, "Hola " .. fromnick .. "!")
		end

	elseif receive:find("PRIVMSG") then
			if false and verbose then
				msg(s, channel, receive)
			end
			if receive:find(channel .. " :") then
				line = string.sub(receive,
					(string.find(receive, channel .. " :") + (#channel) + 2))
			end
			if receive:find(":") and receive:find("!") then
				fromnick = string.sub(receive,
					(string.find(receive, ":") + 1),
					(string.find(receive, "!") - 1))
			end
			if line then
				process(s, channel, fromnick, line, firstword(line) == mynick)
			end
	end
end

function main(arg)
	local serv = arg[1]
	local password = arg[2]
	local channel = "#" .. arg[3]
	local chan_pass = arg[4]

	print("[+] setting up socket to " .. serv)
	s = socket.tcp()
	s:connect(socket.dns.toip(serv), 6667)

	print("[+] authenticating ")
	deliver(s, "PASS " .. password)

	print("[+] trying nick", mynick)
	deliver(s, "NICK " .. mynick)
	deliver(s, "USER " .. mynick .. " " .. " " .. mynick .. " " ..  mynick .. " " .. ":" .. mynick)

	print("[+] joining " .. channel)
	if chan_pass then
		deliver(s, "JOIN " .. channel .. " " .. chan_pass)
	else
		deliver(s, "JOIN " .. channel)
	end

	while true do
		local rcv, err = s:receive('*l')
		if not rcv then
			print("[oops] error: " .. err)
			if err == "closed" then
				return
			end
		else
			pre_process(rcv, channel)
		end
		if verbose then print(rcv) end
	end
end

-- I ain't gonna stop writing C if I can
while true do
	main(arg)
end
