#USAGE:

```
#!bash

lua botella.lua irc.NETWORK-NAME-HERE.org PASSWORD channel [PASSWORD]
```

  **NOTE** that there is NO "#" symbol preceding the channel name.
  This is because "#" would need to be escaped in the shell,
  eg "\#", making an ugly command line interface.
